#!/bin/sh

# SPDX-FileCopyrightText: 2021 Andrea Laisa (amreo) <amreo at linux dot it>
#
# SPDX-License-Identifier: CC0-1.0

if ! type "curl" &> /dev/null; then
  echo "Curl non è installato" >&2
  exit -1
fi

if [[ -f "lista-comuni-temp.csv" ]]; then
    echo "Lista comuni già scaricata in lista-comuni-temp.csv. Download saltato" >&2
else
    echo "Scaricando la lista comuni con curl..." >&2
    curl "https://www.indicepa.gov.it/ipa-dati/dataset/502ff370-1b2c-4310-94c7-f39ceb7500e3/resource/3ed63523-ff9c-41f6-a6fe-980f3d9e501f/download/amministrazioni.txt" | cut -d$'\t' -f1,2,3,9,12 | grep "Comune di" | grep "Comuni e loro Consorzi e Associazioni" | cut -d$'\t' -f1-4 > lista-comuni-temp.csv
fi

echo "Esecuzione del scraper..." >&2
cat lista-comuni-temp.csv | parallel -N1 --pipe  './scrape.py' > out.json 2> err_log.txt