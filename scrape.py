#!/usr/bin/python3

# SPDX-FileCopyrightText: 2021 Andrea Laisa (amreo) <amreo at linux dot it>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys, json, requests, signal, urllib3
from datetime import datetime, timedelta
from http_crawler import crawl
from contextlib import contextmanager
import re

MAX_SCRAPE_TIME = timedelta(minutes=30)
multienteloop = re.compile(r'multiente=([a-z0-9]+)&multiente=\1')
session = requests.Session()
session.headers.update({'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'})
session.verify = False

# Pezzo di codice misterioso copiato da stackoverflow che serve per interrompere i programmi con SIGTERM
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class TimeoutException(Exception): pass
@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)

def detectRealUrl(url: str) -> str:
    try:
        with time_limit(20):
            if url.startswith("http://"):
                # Prova ad verificare che l'url vada bene
                try:
                    newUrl = url
                    if not url.endswith("/"):
                        newUrl += "/"
                    r = session.get(newUrl, timeout=10, verify=False)
                    if r.status_code == 200:
                        return newUrl
                    else:
                        print (newUrl, r.status_code, file=sys.stderr)
                except Exception as ex:
                    print(newUrl, ex, file=sys.stderr)
                    pass

                return "???"
            elif url.startswith("https://"):
                # Prova ad verificare che l'url vada bene
                try:
                    newUrl = url
                    if not url.endswith("/"):
                        newUrl += "/"
                    r = session.get(newUrl, timeout=10, verify=False)
                    if r.status_code == 200:
                        return newUrl
                    else:
                        print(newUrl, r.status_code, file=sys.stderr)
                except Exception as ex:
                    print(newUrl, ex, file=sys.stderr)
                    pass

                return "???"
            else:
                # Prova ad aggiungere https davanti all'url
                try:
                    newUrl = "https://" + url
                    if not url.endswith("/"):
                        newUrl += "/"
                    r = session.get(newUrl, timeout=10, verify=False)
                    if r.status_code == 200:
                        return newUrl
                except Exception as ex:
                    # print(newUrl, ex, file=sys.stderr)
                    pass

                # Prova ad aggiungere http davanti all'url
                try:
                    newUrl = "http://" + url
                    if not url.endswith("/"):
                        newUrl += "/"
                    r = session.get(newUrl, timeout=10)
                    if r.status_code == 200:
                        return newUrl
                    else:
                        print(newUrl, r.status_code, file=sys.stderr)
                except Exception as ex:
                    print(newUrl, ex, file=sys.stderr, flush=True)
                    pass
                return "???"
    except TimeoutException:
        return "???"

def processComune(comune):
    if "sito_web_indicato" not in comune or comune["sito_web_indicato"] == None:
        comune["note"].append("Sito web indicato non valito o assente...")
        return comune
    comune["sito_web_crawlato"] = detectRealUrl(comune["sito_web_indicato"])
    comune["numero_pagine_viste"] = 0
    comune["inizio_crawling"] = datetime.now()
    comune["documenti"] = {
        "pdf": 0,
        "pdf_lista": [],
        "doc": 0,
        "doc_lista": [],
        "docx": 0,
        "docx_lista": [],
        "xls": 0,
        "xls_lista": [],
        "xlsx": 0,
        "xlsx_lista": [],
        "ppt": 0,
        "ppt_lista": [],
        "pptx": 0,
        "pptx_lista": [],
        "odt": 0,
        "odt_lista": [],
        "ods": 0,
        "ods_lista": [],
        "odp": 0,
        "odp_lista": [],
    }
    FORMATI = ["pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "odt", "ods", "odp"]

    if comune["sito_web_crawlato"] != "???":
        print("Start crawling for " + comune["sito_web_crawlato"] , file=sys.stderr, flush=True)
        try:
            with time_limit(int(MAX_SCRAPE_TIME.total_seconds())):
                for rsp in crawl(comune["sito_web_crawlato"], follow_external_links=False, verify=False):
                    if "moondate=" in rsp.url:
                        # Alcuni siti come http://www.comune-sanmarcellopiteglio.info contengono un widget con date future...
                        continue 
                    if "multiente" in rsp.url and multienteloop.search(rsp.url):
                        # Alcuni siti come https://www.comune.collialmetauro.pu.it/c041069/mc/mc_p_ricerca.php?multiente=c041069&multiente=c041069 appendono multiente anche se c'è già...
                        continue
                    if "/verifica_della_rendita_catastal?_w%5Bevent_calendar%5D%5Bd%5D=" in rsp.url:
                        # https://www.comune.carcare.sv.it/verifica_della_rendita_catastal?_w%5Bevent_calendar%5D%5Bd%5D=6880/5
                        continue
                    if "event_listing?mode=week&date=" in rsp.url:
                        # https://www.comune.fiorano-modenese.mo.it/event_listing?mode=week&date=3097-07-13
                        continue
                    comune["numero_pagine_viste"] += 1
                    print('Got {} at {}'.format(rsp.status_code, rsp.url), file=sys.stderr, flush=True)
                    for f in FORMATI:
                        if rsp.url.endswith("." + f):
                            comune["documenti"][f] += 1
                            comune["documenti"][f + "_lista"].append(rsp.url)
        except TimeoutException:
            comune["note"].append("Lo scraping è andato in timeout, probabilmente per un loop o una connessione bloccata")
        except Exception as ex:
            print(comune["sito_web_crawlato"], ex, file=sys.stderr, flush=True)
            pass
    
    comune["fine_crawling"] = datetime.now()
    comune["delta_time_crawling"] = str(comune["fine_crawling"] - comune["inizio_crawling"])
    comune["inizio_crawling"] = str(comune["inizio_crawling"])
    comune["fine_crawling"] = str(comune["fine_crawling"])


    return comune

# Legge tutti i comuni in input in format tsv con cod_amm, des_amm, comune, sito_istituzionale
# https://www.indicepa.gov.it/public-services/docs-read-service.php?dstype=FS&filename=Metadati_Open_Data.pdf
try:
    for line in sys.stdin:
        line = line.strip().split('\t')
        if len(line) < 4:
            continue

        comune = {
            "cod_amm": line[0],
            "comune": line[1],
            "sito_web_indicato": line[3],
            "note": []  
        }

        comune = processComune(comune)
        print(json.dumps(comune), flush=True)
except KeyboardInterrupt:
    pass