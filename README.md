<!--
SPDX-FileCopyrightText: 2021 Andrea Laisa (amreo) <amreo at linux dot it>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# comuni-crawler

Crawler per i comuni italiani scritto per conto di ils

```sh
pip install http-crawler
./run.sh
```